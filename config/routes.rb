Rails.application.routes.draw do
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html

  resources :pages do
  	resources :rows, shallow: true do
  		member do
  			get :toggle_mobile
  			get :toggle_tablet
  			get :toggle_laptop
  			get :toggle_desktop
  			get :toggle_all_devices
  		end
  	end
  end
  root to: 'pages#index'
end
