class PagesController < ApplicationController

	before_action :set_page, only: [:edit, :show]

	def index
		@pages = Page.all
		#@page = Page.create(draft:true)
	end

	def new
		#@page = Page.new
	end

	def create
		@page = Page.create(draft:true)
		redirect_to edit_page_path(@page)
	end

	def edit
		#@page = Page.find(params[:id])
	end

	def update
	end

	def show
		@page = Page.find_by(slug: params[:slug])
	end

	def delete
	end

	private

	def set_page
		@page = Page.includes(:rows).find(params[:id])
	end

	def page_params

	end
end
