class RowsController < ApplicationController

	before_action :set_page, only: [:index, :create]
	before_action :set_row, only: [
	:hide_mobile,
	:toggle_mobile,
	:toggle_tablet,
	:toggle_laptop,
	:toggle_desktop,
	:toggle_all_devices]

	def index
		
	end

	def new; end

	def create
		@row = @page.rows.create

		respond_to do |format|

			format.html { redirect_to edit_page_path(@row.page) }
			format.js
		end
	end

	def edit
		#@page = Page.find(params[:id])
	end

	def update
	end

	def show
		@page = Page.find_by(slug: params[:slug])
	end

	def delete
	end

	def toggle_mobile
		Row.toggle_mobile(@row)
		@page = @row.page
		respond_to do |format|

			format.html {@row}
			format.js
		end
  		
	end

	def toggle_tablet
		Row.toggle_tablet(@row)
		@page = @row.page
		respond_to do |format|

			format.html {@row}
			format.js
		end
  		
	end

	def toggle_laptop
		Row.toggle_laptop(@row)
		@page = @row.page
		respond_to do |format|

			format.html {@row}
			format.js
		end
  		
	end

	def toggle_desktop
		Row.toggle_desktop(@row)
		@page = @row.page
		respond_to do |format|

			format.html {@row}
			format.js
		end
  		
	end

	def toggle_all_devices
		Row.toggle_all_devices(@row)
		@page = @row.page
		respond_to do |format|

			format.html {@row}
			format.js
		end
  		
	end

	private

	def set_row
		@row = Row.find(params[:id])
	end

	def set_page
		@page = Page.find(params[:page_id])
	end

	def row_params

	end

end
