# Place all the behaviors and hooks related to the matching controller here.
# All this logic will automatically be available in application.js.
# You can use CoffeeScript in this file: http://coffeescript.org/

window.rwdscreen = -> 
  breakpoints = 
    'xxs':
      'width': 320
      #'height': 320
      'icon': 'watch'
      'active_class': false
    'xs':
      'width': 480
      #'height': 640
      'icon': 'phone_iphone'
      'active_class': false
    's':
      'width': 768
      #'height': 1024
      'icon': 'tablet_mac'
      'active_class': false
    'm':
      'width': 1024
      #'height': 768
      'icon': 'laptop_mac'
      'active_class': false
    'l':
      'width': 1200
      #'height': 1024
      'icon': 'desktop_windows'
      'active_class': true
    'xl':
      'width': 1600
      #'height': 1200
      'icon': 'tv'
      'active_class': false
  $ ->
    # The DOM is ready!
    # Membas
    deviceCurrent = undefined
    # DOM cache
    $deviceChooser = $('.device-chooser')
    $deviceSize = $('.device-size')
    $content = $('.content')
    $screen = $('#screen')

    $btnMaximize = $('.btn-maximize')
    # URL

    changeDevice = (key, w, h) ->
      ->
        $screen.css('width', w + 'px').css 'height', h + 'px'
        deviceCurrent = key
        #$deviceSize.text w + 'x' + h + 'px'
        $deviceSize.text w + 'px'
        console.log($(this))
        $('.responsive_switcher').removeClass('active') 
        $(this).find('button').addClass('active')
        return

    $screen.on 'change', ->
      # TODO
      console.log 'foo'
      $urlBar.val @attr('src')
      return
    # DEVICES
    $btnMaximize.on 'click', ->
      $content.toggleClass 'content--maximized'
      return
    for key of breakpoints
      `key = key`
      console.log(breakpoints[key]['active_class'])
      #if key == 'l'
      #	console.log('ok')
      btn = $('<li><button class="responsive_switcher"><i class="material-icons">' + breakpoints[key]['icon'] + '</i></button></li>')

      btn.on 'click', changeDevice(key, breakpoints[key]['width'], breakpoints[key]['height'])
      $deviceChooser.append btn
      #console.log(breakpoints[key]);
    initDeviceType = 'l'
    initDevice = changeDevice(initDeviceType, breakpoints[initDeviceType]['width'], breakpoints[initDeviceType]['height'])
    #console.log($('i:contains("desktop_windows")').parent().addClass('active'))
   	$('i:contains("desktop_windows")').parent('button').addClass('active')
    initDevice()
    return
  return



$(document).on 'turbolinks:load', ->
	rwdscreen()
	#console.log()
	return