class Page < ApplicationRecord

	#associations
	has_many :rows
	#callbacks
	before_update :parameterize_slug
	before_update :calculate_slug, if: :uniqness_slug

	def parameterize_slug
		self.slug.parameterize
	end

	def uniqness_slug?
		#self.slug.uniqness?
	end

	def calculate_slug
		number = self.slug.to_i
		if number > 0
			sulg << '_#{number + 1}'
		else
			slug << '_#{}'
		end
	end
end
