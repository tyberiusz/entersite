class Row < ApplicationRecord
  belongs_to :page

  before_create :set_null_html_class

  #before_save :hide_mobile?, :toggle_mobile

  def hide_mobile_icon
  	 if self.html_class.to_s.include? 'hide-on-small-only' 
      'grey-text'
     else
      'white-text'
    end
  end

  def hide_tablet_icon
     if self.html_class.to_s.include? 'hide-on-med-only' 
      'grey-text'
     else
      'white-text'
    end
  end

  def hide_laptop_icon
     if self.html_class.to_s.include? 'hide-on-large-only' 
      'grey-text'
     else
      'white-text'
    end
  end

  def hide_desktop_icon
     if self.html_class.to_s.include? 'hide-on-extra-large-only' 
      'grey-text'
     else
      'white-text'
    end
  end

  def hide_all_devices_icon
    #puts ['hide-on-extra-large-only', 'hide-on-large-only','hide-on-med-only', 'hide-on-small-only'].include? self.html_class.to_s
    # if self.html_class.to_s.include?( 'hide-on-extra-large-only' && 'hide-on-large-only' && 'hide-on-med-only' && 'hide-on-small-only')
    #    'grey-text'
    # else
    #  'white-text'
    #end
  end

  def self.toggle_mobile(row)
    unless row.html_class.to_s.include? 'hide-on-small-only'
      row.html_class << ' hide-on-small-only' 
    else
  	 row.html_class.slice! 'hide-on-small-only'
     #row.html_class.to_s.delete '-'
    end
    row.save!
  end

  def self.toggle_tablet(row)
    unless row.html_class.to_s.include? 'hide-on-med-only'
      row.html_class << ' hide-on-med-only' 
    else
     row.html_class.slice! 'hide-on-med-only'
     #row.html_class.to_s.delete '-'
    end
    row.save!
  end

  def self.toggle_laptop(row)
    unless row.html_class.to_s.include? 'hide-on-large-only'
      row.html_class << ' hide-on-large-only' 
    else
     row.html_class.slice! 'hide-on-large-only'
     #row.html_class.to_s.delete '-'
    end
    row.save!
  end

  def self.toggle_desktop(row)
    unless row.html_class.to_s.include? 'hide-on-extra-large-only'
      row.html_class << ' hide-on-extra-large-only' 
    else
     row.html_class.slice! 'hide-on-extra-large-only'
     #row.html_class.to_s.delete '-'
    end
    row.save!
  end

  def self.toggle_all_devices(row)
    #puts row.html_class.to_s
    puts  row.html_class.to_s.include?( 'hide-on-extra-large-only' || 'hide-on-large-only' || 'hide-on-med-only' || 'hide-on-small-only')
    #puts 'hide-on-extra-large-only'.include? row.html_class.to_s
    unless row.html_class.to_s.include?( 'hide-on-extra-large-only' || 'hide-on-large-only' || 'hide-on-med-only' || 'hide-on-small-only')
      row.html_class << ' hide-on-small-only' 
      row.html_class << ' hide-on-med-only' 
      row.html_class << ' hide-on-large-only' 
      row.html_class << ' hide-on-extra-large-only' 
    else
     row.html_class.slice! 'hide-on-small-only'
     row.html_class.slice! 'hide-on-med-only'
     row.html_class.slice! 'hide-on-large-only'
     row.html_class.slice! 'hide-on-extra-large-only'
    end
    row.save!
  end

  def set_null_html_class
  	self.html_class ||= ''
  end


end
