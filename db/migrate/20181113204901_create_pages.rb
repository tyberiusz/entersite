class CreatePages < ActiveRecord::Migration[5.1]
  def change
    create_table :pages do |t|
      t.string :title
      t.text :meta_description
      t.string :slug, unique: true
      t.boolean :draft

      t.timestamps
    end
  end
end
