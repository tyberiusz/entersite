class CreateRows < ActiveRecord::Migration[5.1]
  def change
    create_table :rows do |t|
      t.references :page, foreign_key: true
      t.integer :order
      t.text :html_class
      t.boolean :hidden

      t.timestamps
    end
  end
end
